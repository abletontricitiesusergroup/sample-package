#Max Sample Package : A plugin for Max/MSP to help you patch faster.

The Max Sample Package is a tool that simplifies some aspects of patching within the Max/MSP environment. The main feature is the ability to quickly connect objects using keyboard shortcuts.

##nstallation

 1. Place the Sample Package folder in one of your search path (Easiest is in "Cycling' 74")
2. Place the Sample Package.maxpat file in your extras folder

To re-install/update the Sample Package

1. Make sure you delete all files related to the Sample Package
2. Install as described above.

##Contact me

Y ou can contact me by email: jonathanmoore [at] computer.org

##Donations

